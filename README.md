# Terminal Setup

Bashrc
* Redirects to profile so all sessions look the same

Bash_Profile
* Runs Prompt and Aliases
* Allows tab completion of SSH aliases from config
* Utility for Mac users

Bash_Prompt
* Stylized prompt including markers for git usage

Bash_Aliases
* Put all aliases here
